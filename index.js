const emptyChar = '.';
const matrixSize = 4;
const maxSumLimit = 2048;
let maxSum = 0;

// this function shifts the array as per the direction and returns the shifted array
function shiftArray(subArr, direction) {
    let newArr = [];
    let bReverse = (direction == 'right' || direction == 'bottom') ? true : false;
    if (bReverse) {
        subArr.reverse();
    }
    //Here items pushed into new array, removing the blank cells and adding the adjacent ones.
    subArr.reduce((acc, itm) => {
        if (itm === emptyChar) {
            return acc;
        }
        let lastVal = acc.length > 0 ? acc.pop() : null;
        // check if last value of new array and itm are equal. If they are equal addition should happen and it should be pushed
        // If not, then itme should be pushed into new array. 
        // dummyValue is used so that consecutive three numbers should not get added. eg: 128,8,4,4 in a right swipe
        if (lastVal === itm) {
            lastVal += itm;
            acc.push(lastVal);
            acc.push('dummyValue');
            maxSum = (maxSum < lastVal) ? lastVal : maxSum;
            if (maxSum === maxSumLimit) {
                gameOver();
            }
        } else if (lastVal === null || lastVal === 'dummyValue') {
            acc.push(itm);
        }
        else {
            acc.push(lastVal);
            acc.push(itm);
        }

        return acc;

    }, newArr);
    //removing the dummy value if it still remained.
    if (newArr[newArr.length - 1] === 'dummyValue') {
        newArr.pop()
    }
    //adding fillers to new array
    while (newArr.length < matrixSize) {
        newArr.push(emptyChar)
    }

    if (bReverse) {
        newArr.reverse();
    }
    return newArr;
}

function gameOver() {
    console.log(`Congratulations you have won this game and reached ${maxSumLimit}`);
    process.exit();
}

function getEmptyIndexes (arr, val) {
    let indexes = [];
    for(let i = 0; i < arr.length; i++)
        if (arr[i] === val) {
            indexes.push(i);
        }
    return indexes;
}

function updateEmptyCellArr(index, shiftedArr, emptyCellArr, direction){
    let emptyIndexes = getEmptyIndexes(shiftedArr, '.');

    for (let i = 0; i < emptyIndexes.length; i++ ) {
        let emptyI = emptyIndexes[i];
        if (direction === 'left' || direction === 'right') {
            emptyCellArr.push(`${index},${emptyI}`);
        } else if (direction === 'top' || direction === 'bottom') {
            emptyCellArr.push(`${emptyI},${index}`);
        }
    }
}

function fillEmptyCell(matrix, emptyCellArr) {
    let length = emptyCellArr.length;
    if (length === 0) {
        return;
    }
    let randomIndex = Math.floor(Math.random() * length);
    let chosenCell = emptyCellArr.splice(randomIndex, 1)[0];
    let [x, y] = chosenCell.split(",");
    matrix[x][y] = (Math.random() >= .5) ? 2 : 4;
}

function displayMatrix(matrix) {
    matrix.map((itmArr) => {
        let str = "";
        itmArr.map((itm) => {
            str += itm + "      ";
        });
        console.log(str);
    })
    console.log("Your input: ");
}

function compareWithOldMatrix(oldAr, newAr) {
    let bEqual = true;
    for (let i = 0; i < oldAr.length; i++) {
        for (let j = 0; j < newAr.length; j++) {
            if (oldAr[i][j] !== newAr[i][j]) {
                bEqual = false;
                break;
            }
        }
        if (!bEqual) {
            break;
        }
    }
    return bEqual;
}

function rightSwipe(matrix, emptyCellArr) {
    //add bolean to identify if swipe 
    emptyCellArr.length = 0; // emptying the array without losing the reference
    for (let i = 0; i < matrixSize; i++) {
        let subArr = matrix[i];
        let shiftedArr = shiftArray(subArr, 'right');
        updateEmptyCellArr(i, shiftedArr, emptyCellArr, 'right');
        matrix[i] = shiftedArr;
    }
}

function bottomSwipe(matrix, emptyCellArr) {
    emptyCellArr.length = 0;
    for (let i = 0; i < matrixSize; i++) {
        let subArr = [];

        //creating column array
        for (let k = 0; k < matrixSize; k++) {
            subArr.push(matrix[k][i]);
        }

        let shiftedArr = shiftArray(subArr, 'bottom');
        updateEmptyCellArr(i, shiftedArr, emptyCellArr, 'bottom');

        shiftedArr.map((itm, index) => {
            matrix[index][i] = itm
        })
    }
}


function leftSwipe(matrix, emptyCellArr) {
    emptyCellArr.length = 0;

    for (let i = 0; i < matrixSize; i++) {
        let subArr = matrix[i];
        let shiftedArr = shiftArray(subArr, 'left');
        updateEmptyCellArr(i, shiftedArr, emptyCellArr, 'left');
        matrix[i] = shiftedArr;
    }
}

function topSwipe(matrix, emptyCellArr) {
    emptyCellArr.length = 0;

    for (let i = 0; i < matrixSize; i++) {
        let subArr = [];
        for (let k = 0; k < matrixSize; k++) {
            subArr.push(matrix[k][i]);
        }

        let shiftedArr = shiftArray(subArr, 'top');
        updateEmptyCellArr(i, shiftedArr, emptyCellArr, 'top');

        shiftedArr.map((itm, index) => {
            matrix[index][i] = itm
        });
    }

}

let input = process.stdin;
input.setEncoding('utf-8');
console.log("Please input text in command line.");
console.log("Press 1 for left swipe");
console.log("Press 2 for top swipe");
console.log("Press 3 for right swipe");
console.log("Press 4 for bottom swipe \n");


let matrix = new Array(matrixSize);
let emptyCellArr = [];

for (let k = 0; k < matrixSize; k++) {
    matrix[k] = new Array(matrixSize).fill(emptyChar)
}

for (let i = 0; i < matrixSize; i++) {
    for (j = 0; j < matrixSize; j++) {
        emptyCellArr.push(`${i},${j}`);
    }
}


fillEmptyCell(matrix, emptyCellArr);
fillEmptyCell(matrix, emptyCellArr);
displayMatrix(matrix);


input.on('data', (data) => {
    console.log("data is", data);
    let matrixClone = matrix.map(inner => inner.slice())

    switch (parseInt(data)) {
        case 1:
            leftSwipe(matrix, emptyCellArr);
            fillEmptyCell(matrix, emptyCellArr);
            displayMatrix(matrix);
            break;
        case 2:
            topSwipe(matrix, emptyCellArr);
            fillEmptyCell(matrix, emptyCellArr);
            displayMatrix(matrix);
            break;
        case 3:
            rightSwipe(matrix, emptyCellArr);
            fillEmptyCell(matrix, emptyCellArr);
            displayMatrix(matrix);
            break;
        case 4:
            bottomSwipe(matrix, emptyCellArr);
            fillEmptyCell(matrix, emptyCellArr);
            displayMatrix(matrix);
            break;
        default:
            console.log("Please enter valid command")
            break;
    }

    let bEqual = compareWithOldMatrix(matrixClone, matrix);
    if (bEqual) {
        console.log("No possible move in this direction. Please try another one")
    }
})